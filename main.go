package main

import (
	"fmt"
	"gorm-db-test/config"
)

var first string
var last string
var grade int

func main() {
	fmt.Println("I am the main() function")
	

	for true {
		fmt.Print("First name: ")
		fmt.Scanln(&first)
		fmt.Print("Last name: ")
		fmt.Scanln(&last)
		fmt.Print("Grade: ")
		fmt.Scanln(&grade)
		fmt.Println(first, last, grade)
		config.CreateRecord(config.DB(), first, last, grade)
	}

}
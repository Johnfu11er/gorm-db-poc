package types

import (
	"gorm.io/gorm"
)

type Student struct {
	gorm.Model
	First				string
	Last				string
	Grade				int
}

type Teacher struct {
	gorm.Model
	First				string
	Last				string
	Subject			string
}
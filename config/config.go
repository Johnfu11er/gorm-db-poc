package config

import (
	"fmt"
	"time"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"gorm-db-test/types"
)


func init() {
	fmt.Println("I am the config init function")
	Migrate(DB())
}


func DB() *gorm.DB {
	fmt.Println("I am the db connection function")
	db, err := gorm.Open(sqlite.Open("classroom.db"), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	
	return db
}


func Migrate(_db *gorm.DB) {
	fmt.Println("I am the config migration function")
	_db.AutoMigrate(
		&types.Student{},
		&types.Teacher{},
	)
}


func CreateRecord(_db *gorm.DB, first string, last string, grade int) {
	fmt.Println("I am the create record function")
	var startTime = time.Now().Local().UnixMilli()
	
	_db.Create(&types.Student{
		First: first,
		Last: last,
		Grade: grade,
	})
	var endTime = time.Now().Local().UnixMilli()
	duration := endTime - startTime
	fmt.Printf("Database write time: %v miliseconds\n\n", duration)
}
